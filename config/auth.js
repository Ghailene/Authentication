module.exports = {

    'facebookAuth' : {
        'clientID'      : '354008948339050', // App ID
        'clientSecret'  : '9ceb8271a80731345270522df41947e3', // App Secret
        'callbackURL'   : 'http://localhost:8080/auth/facebook/callback',
        'profileURL'    : 'http://graph.facebook.com/v2.5/me?fields=first_name,last_name,email',
        'profileFields' : ['id', 'email', 'name'] // For requesting permissions from Facebook API
    },

    'googleAuth' : {
        'clientID'      : '972028666996-l0r99k32h4a39s5jcii6kirunj34atij.apps.googleusercontent.com',
        'clientSecret'  : 'uom6DNwWvXlgEq-P4J8PrGuS',
        'callbackURL'   : 'http://localhost:8080/auth/google/callback'
    }

};
